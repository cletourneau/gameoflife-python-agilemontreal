import itertools


class GameOfLife(object):

    def evolve(self, grid):
        self.grid = grid

        living_cells = filter(self._should_keep, self.grid)
        spawning_cells = filter(self._should_spawn, self._dead_neighbours())

        return list(set(living_cells + spawning_cells))

    def _should_keep(self, cell):
        return len(self._live_neighbours_of(cell)) in (2, 3)

    def _should_spawn(self, cell):
        return len(self._live_neighbours_of(cell)) == 3

    def _dead_neighbours(self):
        dead_neighbours = []
        [self._concatenate(dead_neighbours, self._dead_neighbours_of(c)) for c in self.grid]
        return set(dead_neighbours)

    def _concatenate(self, l1, l2):
        l1 += l2

    def _live_neighbours_of(self, cell):
        return [c for c in (self._neighbours_coordinates_of(cell)) if self._is_live_cell(c)]

    def _dead_neighbours_of(self, cell):
        return [c for c in (self._neighbours_coordinates_of(cell)) if not self._is_live_cell(c)]

    def _neighbours_coordinates_of(self, cell):
        line, column = cell
        return [(line + l, column + c) for l, c in NEIGHBOURS_OFFSET]

    def _is_live_cell(self, coordinate):
        return coordinate in self.grid


NEIGHBOURS_OFFSET = [(-1, -1), (-1, +0), (-1, +1),
                     (+0, -1),           (+0, +1),
                     (+1, -1), (+1, +0), (+1, +1)]

