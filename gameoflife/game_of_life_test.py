import unittest
from gameoflife.game_of_life import GameOfLife
from hamcrest import assert_that, is_


class GameOfLifeTest(unittest.TestCase):
    def test_no_does_not_spawn_by_itself(self):
        evolution = self.evolve([])
        assert_that(evolution, is_(EMPTY))

    def test_no_live_neighbours_dies_of_underpopulation(self):
        evolution = self.evolve([(0, 0)])
        assert_that((0, 0) not in evolution)

    def test_stays_alive_when_it_has_2_neighbours(self):
        evolution = self.evolve([(0, 0), (0, 1), (0, 2)])
        assert_that((0, 1) in evolution)

    def test_stays_alive_when_it_has_3_neighbours(self):
        evolution = self.evolve([(0, 0), (0, 1), (0, 2),
                                         (1, 1)])
        assert_that((1, 1) in evolution)

    def test_with_four_neighbours_dies_of_overpopulation(self):
        evolution = self.evolve([
                                 (1, 0), (1, 1), (1, 2),
                                 (2, 0), (2, 1), (2, 2)])
        assert_that((1, 1) not in evolution)

    def test_spawns_when_it_has_three_neighbours(self):
        evolution = self.evolve([(0, 0), (0, 1),
                                 (1, 0)        ])
        assert_that((1, 1) in evolution)

    def setUp(self):
        self.game_of_life = GameOfLife()
        self.evolve = self.game_of_life.evolve


EMPTY = []